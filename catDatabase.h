///////////////////////////////////////////////////////////////////////////////
/////         University of Hawaii, College of Engineering
///// @brief  Lab 07d - Animal farm 1 - EE 205 - Spr 2022
/////
/////
///// @file catDatabase.h
///// @version 1.0
/////
///// @warning This program must be compiled on the platform it's exploring.
/////
/////
///// @author Jordan Cortado <jcortado@hawaii.edu>
///// @date   02_Mar_2022
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

#define MAX_NAME (50)
#define MAX_CATS (1024)


extern int numberOfCats;


enum Gender { UNKNOWN_GENDER = 0, MALE, FEMALE };

enum Breed { UNKNOWN_BREED , MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX };

enum Color { BLACK, WHITE, RED, BLUE, GREEN, PINK };


struct Cats {
               char                 name[ MAX_NAME ];
               enum Gender          gender;
               enum Breed           breed;
               bool                 isFixed;
               float                weight;
               enum Color           collarColor1;
               enum Color           collarColor2;
               unsigned long long   license;
            };


extern struct Cats cat[];

extern char* sGender ( const enum Gender gender );
extern char* sBreed ( const enum Breed breed );
extern char* sColor ( const enum Color color );

extern bool isWeightValid ( const float weight );
extern bool isNameValid ( const char name[ MAX_NAME ] );
extern bool isIndexValid ( const int index );

