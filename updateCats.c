///////////////////////////////////////////////////////////////////////////////
/////         University of Hawaii, College of Engineering
///// @brief  Lab 07d - Animal farm 1 - EE 205 - Spr 2022
/////
/////
///// @file updateCats.c
///// @version 1.0
/////
///// @warning This program must be compiled on the platform it's exploring.
/////
/////
///// @author Jordan Cortado <jcortado@hawaii.edu>
///// @date   02_Mar_2022
/////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

#include "catDatabase.h"

#include "config.h"

bool updateCatName ( const int index, const char newName[] ) {
   
   if ( isIndexValid(index) == false) {
      return false;
   } 

   if ( isNameValid(newName) == false) {
      return false;
   }

   
   strcpy( cat[index].name, newName );

   printf("\nCat name has been changed to %s. \n", newName);
   return true;
}


bool fixCat( const int index ) {

   if ( isIndexValid(index) == false ) {
      return false;
   }


   cat[index].isFixed = true;
   return true;
}


bool updateCatWeight ( const int index, const float newWeight) {

   if ( isIndexValid(index) == false ) {
      return false;
   }

   if ( isWeightValid(newWeight) == false ) {
      return false;
   }


   cat[index].weight = newWeight;
   return true;
}


bool updateCatCollar1 ( const int index, enum Color newCollarColor1 ) {

   if ( isIndexValid(index) == false ) {
      return false;
   }

   cat[index].collarColor1 = newCollarColor1;
   return true;
}


bool updateCatCollar2 ( const int index, enum Color newCollarColor2 ) {

   if ( isIndexValid(index) == false ) {
      return false;
   }

   cat[index].collarColor2 = newCollarColor2;
   return true;
}


bool updateLicense ( const int index, const unsigned long long newLicense ) {

   if ( isIndexValid(index) == false ) {
      return false;
   }

   cat[index].license = newLicense;
   return true;
}
