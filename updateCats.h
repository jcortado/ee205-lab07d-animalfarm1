///////////////////////////////////////////////////////////////////////////////
/////         University of Hawaii, College of Engineering
///// @brief  Lab 07d - Animal farm 1 - EE 205 - Spr 2022
/////
/////
///// @file updateCats.h
///// @version 1.0
/////
///// @warning This program must be compiled on the platform it's exploring.
/////
/////
///// @author Jordan Cortado <jcortado@hawaii.edu>
///// @date   02_Mar_2022
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>


extern bool updateCatName ( const int index, const char newName[] );

extern bool fixCat ( const int index );

extern bool updateCatWeight ( const int index, const float newWeight);

extern bool updateCatCollar1 ( const int index, enum Color newCollarColor1 );

extern bool updateCatCollar2 ( const int index, enum Color newCollarColor2 );

extern bool updateLicense ( const int index, const unsigned long long newLicense );

